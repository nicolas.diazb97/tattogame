using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class UICharacterStatsHolder : MonoBehaviour
{
    public List<UIStat> stats;

    // Start is called before the first frame update
    void Awake()
    {
        stats = GetComponentsInChildren<UIStat>().ToList();
    }
    private void Start()
    {
        LoadCharacterStats();
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadCharacterStats()
    {
        stats.ForEach(stat =>
        {
            stat.SetCurrentCharacterStat(DataManager.main.GetStatByType(stat.statsType));
        });
    }
    public void ShowTempTattooBoost(TattooData _data)
    {
        stats.ForEach(stat =>
        {
            stat.ResetTempTattooBoost();
            List<SerializableCharacterStat> matchedData = _data.TattooStats.Where(tat => tat.type == stat.statsType).ToList();
            if (matchedData.Count > 0)
            {
                stat.SetCurrentTattooBoost(matchedData[0].value);
            }
        });
    }
}
