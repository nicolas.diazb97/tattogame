using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteManager : MonoBehaviour
{
    public Sprite muted;
    public Sprite unMuted;

    public bool onBattle;
    // Start is called before the first frame update
    void Start()
    {
        if (onBattle)
        {
            AudioManager.main.SetBattle();
        }
        else
        {
            AudioManager.main.SetLobby();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PlayButtonFX()
    {
        GetComponent<AudioSource>().Play();
    }
    public void ChangeAudioState()
    {
        AudioManager.main.ChangeMuteState();
        if (AudioManager.main.onMute)
        {
            GetComponent<Image>().sprite = muted;
        }
        else
        {
            GetComponent<Image>().sprite = unMuted;
        }
    }
}
