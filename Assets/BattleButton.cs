using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleButton : MonoBehaviour
{
    public GameObject error;
    // Start is called before the first frame update
    void Start()
    {
        if (DataManager.main.cards.Count <= 0)
        {
            error.SetActive(true);
            GetComponent<Button>().interactable = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
