using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using System;

public class CardManager : Singleton<CardManager>
{
    public GameObject cardPrefab;
    public GameObject container;
    public OnCardSelected onPowerCardSelected;
    public OnCardSelected onPowerCardDiselected;
    public OnStamineChange onStamineChange;
    public UnityEvent endOfGame;

    public List<PowerCard> selectedCards;
    //public OnCardSelected 
    // Start is called before the first frame update
    void Start()
    {
        LoadData();
        RoundManager.main.onRoundEnd.AddListener((RoundResults _results) =>
        {
            foreach (var selected in selectedCards)
            {
                AnimManager.main.AddAnim(selected.data.animName);
            }
            AnimManager.main.StartPlayingAnims();
        });
        onPowerCardSelected.AddListener((PowerCard _card) =>
        {
            TryToSelectPowerCard(_card);
        });
        onPowerCardDiselected.AddListener((PowerCard _card) =>
        {
            DiselectPowerCard(_card);
        });
    }

    private void DiselectPowerCard(PowerCard card)
    {
        Debug.LogError(card.data.cardName);
        DataManager.main.ReturnStamine(card.data.stamineCost);
        Debug.LogError(DataManager.main.GetCurrentStamine());
        selectedCards.Remove(selectedCards.Where(_card => _card.data.cardName == card.data.cardName).ToList()[0]);
        onStamineChange.Invoke(DataManager.main.GetCurrentStamine());
    }
    public void LoadData()
    {
        DataManager.main.cards.ToList().ForEach(card =>
        {
            GameObject tempuiCard = Instantiate(cardPrefab, container.transform);
            tempuiCard.GetComponent<PowerCard>().SetData(card);
        });

    }
    public void TryToSelectPowerCard(PowerCard _card)
    {
        Debug.LogError("trying to selectec card");
        if (DataManager.main.EnoughStamine(_card.data.stamineCost))
        {
            Debug.Log("stamine: " + DataManager.main.GetCurrentStamine() + " cost: " + _card.data.stamineCost);
            DataManager.main.UseStamine(_card.data.stamineCost);
            selectedCards.Add(_card);
            Debug.LogError("card selected: "+_card.data.cardName);
            Debug.Log("stamine used: " + DataManager.main.GetCurrentStamine());
            onStamineChange.Invoke(DataManager.main.GetCurrentStamine());
        }
        else
        {
            Debug.LogError("not enough stamine, tienes: "+DataManager.main.GetCurrentStamine()+"y cuesta "+_card.data.stamineCost);
        }
    }
    public void SetResults()
    {
        float totalPower = 0;
        float totalEnemyPower = EnemyController.main.GetPowerAttack();
        container.GetComponent<CardHolder>().CheckCardsForCooldown();
        foreach (var _card in selectedCards)
        {
            totalPower += _card.data.power;
            _card.DiselectCard();
        }
        DataManager.main.GetDamaged(totalEnemyPower);
        UIBattleManager.main.UpdateFinalTexts(totalPower.ToString(), totalEnemyPower.ToString());
        EnemyController.main.GetDamaged(totalPower);
        Debug.LogError(DataManager.main.GetCurrentHealth() + " " + EnemyController.main.GetCurrentHealth());
        RoundManager.main.onRoundEnd.Invoke(new RoundResults(DataManager.main.GetCurrentHealth(), EnemyController.main.GetCurrentHealth()));
        selectedCards.Clear();

    }
    public float GetCurrPower()
    {
        float totalPower = 0;
        foreach (var _card in selectedCards)
        {
            totalPower += _card.data.power;
        }
        return totalPower;
    }
}
