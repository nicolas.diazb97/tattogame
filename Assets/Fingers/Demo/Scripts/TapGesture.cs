﻿//
// Fingers Gestures
// (c) 2015 Digital Ruby, LLC
// Source code may be used for personal or commercial projects.
// Source code may NOT be redistributed or sold.
// 

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Events;

namespace DigitalRubyShared
{
    /// <summary>
    /// The DemoScene demo script
    /// </summary>
    public class TapGesture : MonoBehaviour
    {
        /// <summary>
        /// The earth
        /// </summary>
        //public GameObject Earth;

        public GestureEvent OnTap;
        public GestureEvent OnSwipe;


        private TapGestureRecognizer tapGesture;

        private float nextAsteroid = float.MinValue;
        private GameObject draggingAsteroid;

        private readonly List<Vector3> swipeLines = new List<Vector3>();

        private void DebugText(string text, params object[] format)
        {
            //bottomLabel.text = string.Format(text, format);
            Debug.Log(string.Format(text, format));
        }
        private void TapGestureCallback(DigitalRubyShared.GestureRecognizer gesture)
        {
            if (gesture.State == GestureRecognizerState.Ended)
            {
                Debug.Log("Tapped at {0}, {1}"+ gesture.FocusX+ gesture.FocusY);
                OnTap.Invoke(new Vector2(gesture.FocusX, gesture.FocusY));
            }
        }

        private void CreateTapGesture()
        {
            tapGesture = new TapGestureRecognizer();
            tapGesture.StateUpdated += TapGestureCallback;
            //tapGesture.RequireGestureRecognizerToFail = doubleTapGesture;
            FingersScript.Instance.AddGesture(tapGesture);
        }

    
     
        private void RotateGestureCallback(DigitalRubyShared.GestureRecognizer gesture)
        {
            if (gesture.State == GestureRecognizerState.Executing)
            {
                //Earth.transform.Rotate(0.0f, 0.0f, rotateGesture.RotationRadiansDelta * Mathf.Rad2Deg);
            }
        }



        private static bool? CaptureGestureHandler(GameObject obj)
        {
            // I've named objects PassThrough* if the gesture should pass through and NoPass* if the gesture should be gobbled up, everything else gets default behavior
            if (obj.name.StartsWith("PassThrough"))
            {
                // allow the pass through for any element named "PassThrough*"
                return false;
            }
            else if (obj.name.StartsWith("NoPass"))
            {
                // prevent the gesture from passing through, this is done on some of the buttons and the bottom text so that only
                // the triple tap gesture can tap on it
                return true;
            }

            // fall-back to default behavior for anything else
            return null;
        }

        private void Start()
        {

            CreateTapGesture();
            // prevent the one special no-pass button from passing through,
            //  even though the parent scroll view allows pass through (see FingerScript.PassThroughObjects)
            FingersScript.Instance.CaptureGestureHandler = CaptureGestureHandler;

            // show touches, only do this for debugging as it can interfere with other canvases
            FingersScript.Instance.ShowTouches = true;
        }

        private void Update()
        {

#if UNITY_INPUT_SYSTEM_V2

            if (UnityEngine.InputSystem.Keyboard.current.escapeKey.wasPressedThisFrame)

#else

            if (UnityEngine.Input.GetKeyDown(KeyCode.Escape))

#endif

            {
                ReloadDemoScene();
            }
        }

        private void LateUpdate()
        {
           

            int touchCount = UnityEngine.Input.touchCount;
            if (FingersScript.Instance.TreatMousePointerAsFinger && UnityEngine.Input.mousePresent)
            {
                touchCount += (UnityEngine.Input.GetMouseButton(0) ? 1 : 0);
                touchCount += (UnityEngine.Input.GetMouseButton(1) ? 1 : 0);
                touchCount += (UnityEngine.Input.GetMouseButton(2) ? 1 : 0);
            }
            string touchIds = string.Empty;
            int gestureTouchCount = 0;
            foreach (DigitalRubyShared.GestureRecognizer g in FingersScript.Instance.Gestures)
            {
                gestureTouchCount += g.CurrentTrackedTouches.Count;
            }
            foreach (GestureTouch t in FingersScript.Instance.CurrentTouches)
            {
                touchIds += ":" + t.Id + ":";
            }

        }

        // draw that last few swipes
        
        /// <summary>
        /// Reload the demo scene
        /// </summary>
        public void ReloadDemoScene()
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0, UnityEngine.SceneManagement.LoadSceneMode.Single);
        }
    }
}
