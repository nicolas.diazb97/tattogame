using UnityEngine;

[CreateAssetMenu(fileName = "TattoosData", menuName = "TattoosData", order = 1)]
public class TattoosData : ScriptableObject
{
    public TattooData[] tattoos;
}