using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PaintIn3D
{
    public class TattooingManager : Singleton<TattooingManager>
    {
        public P3dPaintDecal tattooMachine;
        // Start is called before the first frame update
        void Start()
        {
            ShopManager.main.OnTattooSelected.AddListener((TattooData _data) =>
            {
                Debug.Log(_data);
                SetTattooOnMachine(_data.texture);
            });

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void SetTattooOnMachine(Texture _texture)
        {
            tattooMachine.Texture = _texture;
        }
    }
}
