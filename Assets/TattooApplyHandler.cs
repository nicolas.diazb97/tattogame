using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace PaintIn3D
{
    public class TattooApplyHandler : Singleton<TattooApplyHandler>
    {
        public P3dHitScreen tattooMachine;
        public P3dPaintableTexture tattooTexture;
        public OnTattoApply onTattoApply;
        public UnityEvent onTattoApplyFail;
        public TattooData currSelectedTattoo;
        public OnTextureSaved onTextureSaved;
        // Start is called before the first frame update
        void Start()
        {
            ShopManager.main.onTattoStateChecked.AddListener((bool _state) =>
            {
                tattooMachine.canTattoo = _state;
            });
            ShopManager.main.OnTattooSelected.AddListener((TattooData _data) =>
            {
                currSelectedTattoo = _data;
            });
            tattooMachine.OnTattooApply.AddListener(() =>
            {
                Debug.LogError("esto pasaaaaa");
                onTattoApply.Invoke(currSelectedTattoo);
                DataManager.main.AddNewTattooOnSkin(currSelectedTattoo.tattooName);
                tattooTexture.Save();
            });
            tattooMachine.OnTattooApplyFail.AddListener(() =>
            {
                onTattoApplyFail.Invoke();
            });
            tattooTexture.onTextureSaved.AddListener(() =>
            {
                onTextureSaved.Invoke(new LoadableTattooDataTexture(tattooTexture.savedName, tattooTexture.pngData));
                Debug.LogError("esto dfghj ");
                DatabaseManager.main.SetUserData(DatabaseManager.main.userID, tattooTexture.pngData, tattooTexture.savedName, DataManager.main.tattoosOnSkinNames.ToArray(), DataManager.main.ownedTattoos.ToArray());
            });
            tattooTexture.LoadFromDB(DatabaseManager.main.pngData);
        }

        // Update is called once per frame
        void Update()
        {
        }
    }
}
