using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Firebase.Auth;

public class SignUp : MonoBehaviour
{
    public UnityEvent OnSigned;
    // Start is called before the first frame update
    void Start()
    {
        DatabaseManager.main.OnSignUp.AddListener((Firebase.Auth.FirebaseUser newUser) =>
        {
            OnSigned.Invoke();
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
