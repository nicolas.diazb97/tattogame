using Firebase.Auth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using Firebase.Extensions;
using UnityEngine.SceneManagement;
using Firebase.Database;


public class DatabaseManager : Singleton<DatabaseManager>
{
    private FirebaseAuth auth;
    private FirebaseUser user;
    public string userID;
    public string displayName;
    public string emailAddress;
    public string photoUrl;
    public string pngData;
    public OnUserAuth OnSignUp;
    public UnityEvent OnPasswordReset;
    bool newuser = false;
    DatabaseReference reference;
    // Start is called before the first frame update
    void Start()
    {
        InitializeFirebase();
        OnSignUp.AddListener((Firebase.Auth.FirebaseUser newUser) =>
        {
            displayName = newUser.DisplayName;
            emailAddress = newUser.Email;
            userID = newUser.UserId;
        });
        reference = FirebaseDatabase.DefaultInstance.RootReference;

        //WriteNewUser("prueba", "nickname prueba", "email prueba", "nodata", DataManager.main.buyedTattoos.ToArray());
        //TryToAuthWithEmail("nicolas.diazb97@gmail.com", "Nico12las");
        //SendVerificationEmail();
        //TryToCreateUserWithEmail("nicolas.diazb97@gmail.com", "Nico12las");
    }
    void InitializeFirebase()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
        Debug.LogError("Firebase init");
    }
    private void WriteNewUser(string userId, string name, string email, string _texture, string _name, string[] _dataOnSkin, string[] _data)
    {
        User user = new User(name, email, _texture, _name, userId, _dataOnSkin, _data);
        string json = JsonUtility.ToJson(user);

        FirebaseDatabase.DefaultInstance
 .GetReference("users").Child(userId).SetRawJsonValueAsync(json);
    }
    public void SendVerificationEmail()
    {
        Firebase.Auth.FirebaseUser user2 = auth.CurrentUser;
        if (user != null)
        {
            user.SendEmailVerificationAsync().ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("SendEmailVerificationAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("SendEmailVerificationAsync encountered an error: " + task.Exception);
                    return;
                }

                Debug.Log("Email sent successfully.");
            });
        }
    }
    public void ResetPassword(string _emailToRecover)
    {
        string emailAddress = _emailToRecover;
        auth.SendPasswordResetEmailAsync(emailAddress).ContinueWithOnMainThread(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SendPasswordResetEmailAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SendPasswordResetEmailAsync encountered an error: " + task.Exception);
                return;
            }

            Debug.Log("Password reset email sent successfully.");
            OnPasswordReset.Invoke();
        });
    }
    public void UpdateUserData(string _displayName)
    {
        Firebase.Auth.FirebaseUser user = auth.CurrentUser;
        if (user != null)
        {
            Firebase.Auth.UserProfile profile = new Firebase.Auth.UserProfile
            {
                DisplayName = _displayName,
                PhotoUrl = new System.Uri("https://example.com/jane-q-user/profile.jpg"),
            };
            user.UpdateUserProfileAsync(profile).ContinueWithOnMainThread(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("UpdateUserProfileAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("UpdateUserProfileAsync encountered an error: " + task.Exception);
                    return;
                }

                Debug.Log("User profile updated successfully.");

                displayName = user.DisplayName;
                userID = user.UserId;
                WriteNewUser(user.UserId, user.DisplayName, user.Email, "nopngdata", "no name", DataManager.main.tattoosOnSkinNames.ToArray(), DataManager.main.ownedTattoos.ToArray());

                SetUserTextureByID("nopngData");
                SceneManager.LoadScene("Home");
            });
        }
    }
    public void SetUserData(string _hash, string _tattooTexture, string _name, string[] _tattoosOnSkin, string[] _buyedTattoos)
    {
        Debug.LogError("tattoos data setted");
        reference.Child("users").Child(_hash).Child("tattoosTexture").SetValueAsync(_tattooTexture);
        reference.Child("users").Child(_hash).Child("textureName").SetValueAsync(_name);
        reference.Child("users").Child(_hash).Child("buyedTattoosData").SetValueAsync(_buyedTattoos);
        reference.Child("users").Child(_hash).Child("tattoosOnSkin").SetValueAsync(_tattoosOnSkin);
    }
    public void LoadBuyedTattoos()
    {
        string[] buyedTattoos = new string[0];
        Debug.LogError("askinf for firebase");
        FirebaseDatabase.DefaultInstance
  .GetReference("users").Child(user.UserId).Child("buyedTattoosData")
  .GetValueAsync().ContinueWithOnMainThread(task =>
  {
      if (task.IsFaulted)
      {
          //return "error";9
          // Handle the error...
          buyedTattoos = new string[0];
      }
      else if (task.IsCompleted)
      {
          DataSnapshot snapshot = task.Result;
          List<string> dictUsers = new List<string>();
          foreach (DataSnapshot s in snapshot.Children)
          {
              dictUsers.Add(s.Value.ToString());
          }
          buyedTattoos = dictUsers.ToArray();
          DataManager.main.ownedTattoos = dictUsers;
          //if (task.Result.Child("state").Value != null)
          //Debug.Log("state:" + task.Result.Child("state").Value.ToString());
      }
  });
        
    }
    public void SetUserTextureByID(string _hash)
    {
        FirebaseDatabase.DefaultInstance
  .GetReference("users").Child(_hash).Child("tattoosTexture")
  .GetValueAsync().ContinueWithOnMainThread(task =>
  {
      if (task.IsFaulted)
      {
          //return "error";9
          // Handle the error...
      }
      else if (task.IsCompleted)
      {
          DataSnapshot snapshot = task.Result;
          pngData = snapshot.Value.ToString();
          //if (task.Result.Child("state").Value != null)
          //Debug.Log("state:" + task.Result.Child("state").Value.ToString());
          // Do something with snapshot...
      }
  });
    }
    public void SetUserTattoosByID(string _hash, string _buyedTattoos, string _tattooosOnSkin)
    {
        FirebaseDatabase.DefaultInstance
  .GetReference("users").Child(_hash).Child("buyedTattoos")
  .GetValueAsync().ContinueWithOnMainThread(task =>
  {
      if (task.IsFaulted)
      {
          //return "error";9
          // Handle the error...
      }
      else if (task.IsCompleted)
      {
          DataSnapshot snapshot = task.Result;
          pngData = snapshot.Value.ToString();
          //if (task.Result.Child("state").Value != null)
          //Debug.Log("state:" + task.Result.Child("state").Value.ToString());
          // Do something with snapshot...
      }
  });
    }

    public void TryToCreateUserWithEmail(string _email, string _password)
    {
        //Cuando un usuario nuevo se registre mediante el formulario de registro de la app, realiza los pasos de 
        //validación de la cuenta nueva necesarios, como verificar que se haya escrito correctamente la contraseña 
        //y que cumpla con los requisitos de complejidad.
        newuser = true;
        auth.CreateUserWithEmailAndPasswordAsync(_email, _password).ContinueWithOnMainThread(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            // Firebase user has been created.
            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);
            OnSignUp.Invoke(newUser);
            Debug.LogError(auth.CurrentUser.Email);
        });
    }
    public void SignOut()
    {
        auth.SignOut();
        SceneManager.LoadScene("Login");
    }

    public void TryToAuthWithEmail(string _email, string _password)
    {
        auth.SignInWithEmailAndPasswordAsync(_email, _password).ContinueWithOnMainThread(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                UILoginManager.main.SetError("*incorrect password");
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                newUser.DisplayName, newUser.UserId);

            //WriteNewUser(newUser.UserId, newUser.DisplayName, newUser.Email, "nodata", DataManager.main.buyedTattoos.ToArray());

            SceneManager.LoadScene("Home");
        });
    }
    public void UpdateLocalUserTextureFromDB()
    {
        SetUserTextureByID(user.UserId);
    }
    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                Debug.Log("Signed in " + user.UserId);
                displayName = user.DisplayName ?? "";
                emailAddress = user.Email ?? "";
                userID = user.UserId ?? "";
                if (!newuser)
                {
                    SceneManager.LoadScene("Home");
                }
            }
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}
