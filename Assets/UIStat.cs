using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStat : MonoBehaviour
{
    public Slider characterStatsList;
    public Slider tattooStats;
    public SerializableCharacterStat.characterStats statsType;

    private float currCharacterStat;
    private float currTattooBoost;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetCurrentCharacterStat(SerializableCharacterStat _stat)
    {
        currCharacterStat = _stat.value;
        characterStatsList.value = _stat.value.Remap(0, (int)_stat.type, 0.000f, 1.000f);
    }
    public void SetCurrentTattooBoost(float _boost)
    {
        currTattooBoost = _boost;
        tattooStats.value = (_boost + currCharacterStat).Remap(0, (int)statsType, 0.000f, 1.000f);
    }
    public void ResetTempTattooBoost()
    {
        currTattooBoost = 0;
        tattooStats.value = 0;
    }

}
