using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    public AudioClip lobby;
    public AudioClip battle;

    public bool onMute;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetBattle()
    {
        GetComponent<AudioSource>().clip = battle;
        GetComponent<AudioSource>().Play();
    }
    public void SetLobby()
    {
        GetComponent<AudioSource>().clip = lobby;
        GetComponent<AudioSource>().Play();
    }
    public void ChangeMuteState()
    {
        if (onMute)
        {
            GetComponent<AudioSource>().mute = false;
            onMute = false;
        }
        else
        {
            GetComponent<AudioSource>().mute = true;
            onMute = true;
        }
    }
}
