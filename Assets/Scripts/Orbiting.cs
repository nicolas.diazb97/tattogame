using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbiting : GameState
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Debug.Log("Mouse is pressed down");

            //RaycastHit hitInfo = new RaycastHit();
            //if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo))
            //{
            //    Debug.Log("Object Hit is " + hitInfo.collider.gameObject.name);

            //    //If you want it to only detect some certain game object it hits, you can do that here
            //    if (hitInfo.collider.gameObject.GetComponent<BodyPart>())
            //    {
            //        hitInfo.collider.gameObject.GetComponent<BodyPart>().Interact();
            //        //do something to dog here
            //    }
            //}
        }
    }
    public void Tapped(Vector2 _tapOrigin)
    {
        Debug.Log("Tapped received");
        RaycastHit hitInfo = new RaycastHit();
        if (Physics.Raycast(Camera.main.ScreenPointToRay(_tapOrigin), out hitInfo))
        {
            Debug.Log("Object Hit is " + hitInfo.collider.gameObject.name);

            //If you want it to only detect some certain game object it hits, you can do that here
            if (hitInfo.collider.gameObject.GetComponent<BodyPart>())
            {
                hitInfo.collider.gameObject.GetComponent<BodyPart>().Interact();
                //do something to dog here
            }
        }
    }

}