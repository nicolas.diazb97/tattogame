using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerCard : MonoBehaviour
{
    public SerializablePowerCard data;
    public Image mainImage;
    public bool selected;
    public bool canBeSelected = true;
    private int cooldownRoundLeft = 0;
    // Start is called before the first frame update
    void Start()
    {
        CardManager.main.onStamineChange.AddListener((float _stamine) =>
        {
            if (!selected && canBeSelected)
            {
                Debug.Log("esto pasaaaa" + _stamine);
                if (data.stamineCost > _stamine)
                {
                    Debug.Log("no alcanza");
                    GetComponent<Button>().interactable = false;
                }
                else
                {
                    Debug.LogError("tienes "+_stamine+" alcanza para: " + data.cardName + " que cuesta "+ data.stamineCost);
                    GetComponent<Button>().interactable = true;
                }
            }
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetData(SerializablePowerCard _data)
    {
        data = _data;
        FillPowerCard();
    }
    public void FillPowerCard()
    {
        mainImage.sprite = data.cardIllustration;
        GetComponentInChildren<PowerCardFrame>().GetComponent<Image>().sprite = data.cardFrame;
        GetComponent<Button>().onClick.AddListener(() =>
        {
            Interact();
        });
    }
    public void Interact()
    {
        Debug.LogError("Interact");
        if (!selected)
        {
            Debug.LogError("Selected");
            selected = true;
            GetComponent<Animator>().SetBool("Selected", true);
            GetComponentInChildren<Selector>().GetComponent<Image>().enabled = true;
            CardManager.main.onPowerCardSelected.Invoke(this);
            //ShopManager.main.OnTattooSelected.Invoke(data);
        }
        else
        {
            Debug.LogError("Diselected");
            DiselectCard();
            CardManager.main.onPowerCardDiselected.Invoke(this);
        }
    }
    public void CheckForCooldown()
    {
        Debug.LogError("checking for cooldown");
        if (selected || cooldownRoundLeft > 0)
        {
            Debug.LogError("esto pasa");
            //primera ronda en cooldown
            if (canBeSelected)
            {
                cooldownRoundLeft = data.roundsCooldown;
            }
            if (cooldownRoundLeft > 0)
            {
                Debug.LogError(cooldownRoundLeft + " rondas restantes");
                canBeSelected = false;
                GetComponentInChildren<CooldownTimer>(true).gameObject.SetActive(true);
                GetComponent<Button>().interactable = false;
                cooldownRoundLeft--;
            }
        }
        else
        {
            canBeSelected = true;
            GetComponent<Button>().interactable = true;
            GetComponentInChildren<CooldownTimer>(true).gameObject.SetActive(false);
        }
    }
    public void DiselectCard()
    {
        selected = false;
        GetComponent<Animator>().SetBool("Selected", false);
        GetComponentInChildren<Selector>().GetComponent<Image>().enabled = false;
    }
}

public static class ConvertToSpriteExtensiton
{
    public static Sprite ConvertToSprite(this Texture2D texture)
    {
        return Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
    }
}