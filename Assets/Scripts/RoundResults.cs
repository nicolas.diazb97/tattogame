﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class RoundResults
{
    [SerializeField]
    public float currHealth;
    [SerializeField]
    public float currEnemyHealth;
    [SerializeField]
    public RoundResults(float _health, float _enemyHealth)
    {
        currHealth = _health;
        currEnemyHealth = _enemyHealth;
    }
}