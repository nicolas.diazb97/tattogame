using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DataManager : Singleton<DataManager>
{
    public TattoosData globalTattooData;

    public DataToStore gameData;

    public List<SerializableCharacterStat> characterStats;
    public List<SerializablePowerCard> cards;
    public List<TattooData> tattoosOnSkin;
    public List<string> ownedTattoos;
    public List<string> tattoosOnSkinNames;
    public int currMoney;

    public float currStamine;
    public float currHealth;

    public string characterName;
    private void Start()
    {
        Debug.LogError(Application.persistentDataPath);
        gameData = Serializer.Load<DataToStore>(Application.persistentDataPath + "/GameData.cs");
        if (gameData != null)
        {
            GetLocalData();
        }
        else
        {
            gameData = new DataToStore();
        }
    }

    public void GetLocalData()
    {
        DataToStore localData = Serializer.Load<DataToStore>(Application.persistentDataPath + "/Gamedata.cs");
        characterStats = localData.characterStats;
        cards.Clear();
        tattoosOnSkin.Clear();
        foreach (var _localTat in localData.tattoosOnSkin)
        {
            tattoosOnSkin.Add(globalTattooData.tattoos.Where(_tat => _tat.tattooName == _localTat).ToList()[0]);
            cards.Add(globalTattooData.tattoos.Where(_tat => _tat.tattooName == _localTat).ToList()[0].powerCard);
        }
        //buyedTattoos = localData.buyedTattoos;
        tattoosOnSkinNames = localData.tattoosOnSkin;
        currMoney = localData.currMoney;
        currStamine = localData.currStamine;
        currHealth = localData.currHealth;
        characterName = localData.characterName;
        Debug.LogError("data setted");
    }

    public void GetDatafromDB()
    {
        DatabaseManager.main.LoadBuyedTattoos();
        Debug.LogError("data setted from db");
    }
    public void UpdateGameData()
    {
        if (gameData != null && characterStats != null)
        {
            gameData.characterStats = characterStats;
            gameData.cards = new List<string>();
            foreach (var _card in cards)
            {
                gameData.cards.Add(_card.cardName);

            }
            gameData.tattoosOnSkin = new List<string>();
            foreach (var _tatOnSkin in tattoosOnSkin)
            {
                gameData.tattoosOnSkin.Add(_tatOnSkin.tattooName);

            }
            //gameData.buyedTattoos = buyedTattoos;
            gameData.currMoney = currMoney;
            gameData.currStamine = currStamine;
            gameData.currHealth = currHealth;
            gameData.characterName = characterName;
            Serializer.Save<DataToStore>(Application.persistentDataPath + "/GameData.cs", gameData);
            Debug.Log("GameData update succesfully");
        }
        else
        {
            Debug.LogError("volvio a ocurrir este error culero");
        }
    }
    // Start is called before the first frame update
    public void InitBattleValues()
    {
        currStamine = characterStats.Where(stat => stat.type == SerializableCharacterStat.characterStats.stamine).ToList()[0].value;
        currHealth = characterStats.Where(stat => stat.type == SerializableCharacterStat.characterStats.health).ToList()[0].value;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public float GetCurrentStamine()
    {
        return currStamine;
    }
    public float GetCurrentHealth()
    {
        return currHealth;
    }
    public void GetDamaged(float _power)
    {
        currHealth -= _power;
    }
    public bool EnoughStamine(float _cost)
    {
        return (currStamine >= _cost) ? true : false;
    }
    public void UseStamine(float _cost)
    {
        currStamine -= _cost;
    }
    public void ReturnStamine(float _cost)
    {
        currStamine += _cost;
    }
    public SerializableCharacterStat GetStatByType(SerializableCharacterStat.characterStats _stat)
    {
        Debug.Log(_stat);
        return characterStats.Where(stat => stat.type == _stat).ToList()[0];
    }
    public void UpdateChracter(TattooData _data)
    {
        List<TattooData> matchedTattoos = tattoosOnSkin.Where(tat => tat.tattooName == _data.tattooName).ToList();
        if (matchedTattoos.Count <= 0)
        {
            foreach (var tatStat in _data.TattooStats)
            {
                UpdateStats(tatStat);

            }
            AddNewCard(_data.powerCard);
            tattoosOnSkin.Add(_data);
        }
        //UpdateGameData();
    }
    public bool CheckForOwnedTattooByName(string _name)
    {
        List<string> matchedTattoos = ownedTattoos.Where(tat => tat == _name).ToList();
        if (matchedTattoos.Count > 0)
        {
            //Debug.LogError("checking for owned tattoo with a result of: "+true);
            return true;
        }
        else
        {
            //Debug.LogError("checking for owned tattoo with a result of: "+false);
            return false;
        }
    }
    public void ResetStamine()
    {
        currStamine = characterStats.Where(stat => stat.type == SerializableCharacterStat.characterStats.stamine).ToList()[0].value;
    }
    public void AddNewOwnedTattoo(string _name)
    {
        List<string> matchedTattoos = ownedTattoos.Where(tat => tat == _name).ToList();
        if (matchedTattoos.Count > 0)
        {

        }
        else
        {
            ownedTattoos.Add(_name);
        }
    }
    public void AddNewTattooOnSkin(string _name)
    {
        List<string> matchedTattoos = tattoosOnSkinNames.Where(tat => tat == _name).ToList();
        if (matchedTattoos.Count > 0)
        {

        }
        else
        {
            tattoosOnSkinNames.Add(_name);
        }
    }
    void UpdateStats(SerializableCharacterStat _stat)
    {
        GetStatByType(_stat.type).value += _stat.value;
    }
    void AddNewCard(SerializablePowerCard _card)
    {
        Debug.LogError("se agrega una nueva carta " + _card.cardName);
        cards.Add(_card);
    }
    public bool CheckEnoughMoney(int _cost)
    {
        return (currMoney >= _cost) ? true : false;
    }
    public void AddMoney(int _moneyToAdd)
    {
        currMoney += _moneyToAdd;
    }
    public void SpendMoney(int _moneyToSpend)
    {
        currMoney -= _moneyToSpend;
    }
}
public static class ExtensionMethods
{

    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

}
