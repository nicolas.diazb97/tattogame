using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BodyPartsManager : Singleton<BodyPartsManager>
{
    public List<BodyPart> parts;
    // Start is called before the first frame update
    void Start()
    {
        parts = GetComponentsInChildren<BodyPart>(true).ToList();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetPartsState(bool _state)
    {
        parts.ForEach(p => p.gameObject.SetActive(_state));
    }
}
