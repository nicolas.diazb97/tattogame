using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPart : MonoBehaviour
{
    public float zoom;
    public float minZoom;
    public float maxZoom = 1.6f;
    public GameObject backButton;
    public GameObject orbitButton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Interact()
    {
        SmoothOrbitCam.main.EnableOrbiting = true;
        SmoothOrbitCam.main.enablePanning = true;
        SmoothOrbitCam.main.enableZooming = true;
        SmoothOrbitCam.main.distance = zoom;
        SmoothOrbitCam.main.distanceMin = minZoom;
        SmoothOrbitCam.main.distanceMax = maxZoom;
        SmoothOrbitCam.main.target = this.transform;
        backButton.SetActive(true);
        orbitButton.SetActive(true);
        BodyPartsManager.main.SetPartsState(false);
    }
    public void Reset()
    {
        backButton.SetActive(false);

    }
}
