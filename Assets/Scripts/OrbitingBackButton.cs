using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitingBackButton : MonoBehaviour
{

    public float zoom;
    public float minZoom;
    public GameObject target;
    public GameObject orbitButtons;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Interact()
    {
        SmoothOrbitCam.main.EnableOrbiting = true;
        SmoothOrbitCam.main.enablePanning = false;
        SmoothOrbitCam.main.enableZooming = false;
        SmoothOrbitCam.main.distance = zoom;
        SmoothOrbitCam.main.distanceMin = minZoom;
        SmoothOrbitCam.main.distanceMax = 8;
        SmoothOrbitCam.main.target = target.transform;
        orbitButtons.SetActive(false);
        BodyPartsManager.main.SetPartsState(true);
    }
}
