using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataToStore 
{
    [SerializeField]
    public List<SerializableCharacterStat> characterStats;
    [SerializeField]
    public List<string> cards;
    [SerializeField]
    public List<string> tattoosOnSkin;
    [SerializeField]
    public List<string> buyedTattoosData;
    [SerializeField]
    public int currMoney;

    [SerializeField]
    public float currStamine;
    [SerializeField]
    public float currHealth;
    [SerializeField]
    public string characterName;
}
