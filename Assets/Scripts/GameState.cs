using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameState : MonoBehaviour
{
    public UnityEvent OnStart;
    public UnityEvent OnEnded;
    public GamePlayStates stateName;
    public enum GamePlayStates
    {
        orbiting = 0,
        stencil = 1,
        tattooing =3
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

}
