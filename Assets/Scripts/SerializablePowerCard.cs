using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SerializablePowerCard
{
    [SerializeField]
    public string cardName;

    [SerializeField]
    public Sprite cardIllustration;
    
    [SerializeField]
    public string animName;
    
    [SerializeField]
    public float power;

    [SerializeField]
    public float stamineCost;

    [SerializeField]
    public int roundsCooldown;

    [SerializeField]
    public SerializableCharacterStat[] tempStatsBoost;

    [SerializeField]
    public Sprite cardFrame;

}
