using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class User
{
    public string username;
    public string email;
    public string textureName;
    public string tattoosTexture;
    public string userID;
    public string[] buyedTattoosData;
    public string[] tattoosOnSkinData;

    public User()
    {
    }

    public User(string username, string email, string _pngData, string _name, string _userID, string[] _tattoosOnSkin, string[] _buyedTattoosData)
    {
        this.username = username;
        this.email = email;
        this.textureName = _name;
        this.tattoosTexture = _pngData;
        this.userID = _userID;
        this.buyedTattoosData = _buyedTattoosData;
        this.tattoosOnSkinData = _tattoosOnSkin;
    }
}
