using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TattooData 
{
    [SerializeField]
    public string tattooName;
    [SerializeField]
    public string tattooDescription;
    [SerializeField]
    public tattoStyles tattoosGender;
    [SerializeField]
    public enum tattoStyles
    {
        oriental = 0,
        ancestral = 1,
        realism = 2,
        traditional = 3
    }
    [SerializeField]
    public SerializableCharacterStat[] TattooStats;
    [SerializeField]
    public Texture texture;
    [SerializeField]
    public SerializablePowerCard powerCard;
    [SerializeField]
    public int cost;
}
