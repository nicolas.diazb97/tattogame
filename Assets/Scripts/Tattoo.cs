using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tattoo : MonoBehaviour
{
    public TattooData data;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetData(TattooData _data)
    {
        data = _data;
        FillShopUI();
    }
    public void FillShopUI()
    {
        GetComponentInChildren<RawImage>().texture = data.texture;
        GetComponent<Button>().onClick.AddListener(() =>
        {
            SetTattooTexture();
        });
    }
    public void SetTattooTexture()
    {
        ShopManager.main.OnTattooSelected.Invoke(data);
    }
}


