using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    public GameState currGameState;
    public List<GameState> gameStates;
    // Start is called before the first frame update
    void Start()
    {
        gameStates = GetComponentsInChildren<GameState>(true).ToList();
        SetNewGameStateByName(GameState.GamePlayStates.orbiting);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetNewGameStateByName(GameState.GamePlayStates _newStateName)
    {
        if (currGameState != null)
        {
            currGameState.OnEnded.Invoke();
        }
        currGameState = gameStates.Where(gt=>gt.stateName == _newStateName).ToList()[0];
        currGameState.OnStart.Invoke();
    }
}
