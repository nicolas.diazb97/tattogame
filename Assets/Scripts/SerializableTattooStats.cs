using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SerializableTattooStats
{
    [SerializeField]
    public float magic;
    [SerializeField]
    public float force;    
    [SerializeField]
    public float shield;    
    [SerializeField]
    public float stamine;    
    [SerializeField]
    public float health;
}
