using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SerializableCharacterStat
{
    [SerializeField]
    public characterStats type;
    [SerializeField]
    public float value;
    [SerializeField]
    public enum characterStats
    {
        magic = 500,
        force = 450,
        shield = 490,
        stamine = 400,
        health = 1000
    }
}
