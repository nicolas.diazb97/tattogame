using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TattooingBackButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Interact()
    {
        GameManager.main.SetNewGameStateByName(GameState.GamePlayStates.orbiting);
        BodyPartsManager.main.SetPartsState(true);
    }
}
