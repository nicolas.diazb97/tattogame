using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Round : MonoBehaviour
{
    public string roundName;
    public RoundResults results;
    // Start is called before the first frame update
    float timeRemaining = 10;
    public bool timerIsRunning = false;

    private void Start()
    {
        // Starts the timer automatically
        timerIsRunning = true;
    }

    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
            }
            else
            {
                Debug.Log("Time has run out!");
                CardManager.main.SetResults();
                 timeRemaining = 0;
                timerIsRunning = false;
            }
            DisplayTime(timeRemaining);
        }
    }
    void DisplayTime(float timeToDisplay)
    {
        //Debug.Log(timeToDisplay);
        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);
        UIBattleManager.main.SetTimer(timeToDisplay.Remap( 0.0000f, RoundManager.main.roundDuration,0.0000f, 1.0000f));
        //timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
    public void Init(float _duration)
    {
        gameObject.SetActive(true);
        UIBattleManager.main.UpdateRoundData(roundName);
        timeRemaining = _duration;
    }
    public void SetResults(RoundResults _results)
    {
        results = _results;
        DataManager.main.ResetStamine();
        CardManager.main.onStamineChange.Invoke(DataManager.main.GetCurrentStamine());
    }
}
