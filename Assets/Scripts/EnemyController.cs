using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyController : Singleton<EnemyController>
{
    public List<SerializableCharacterStat> characterStats;
    public List<SerializablePowerCard> cards;

    public float currStamine;
    public float currHealth;
    public List<SerializablePowerCard> selectedEnemyCards;
    public SkinnedMeshRenderer torso;
    public List<TattooData> tattoos;
    public List<Texture2D> opcs;

    public string characterName;
    // Start is called before the first frame update
    void Start()
    {


    }
    public void InitBaseValues()
    {
        currStamine = characterStats.Where(stat => stat.type == SerializableCharacterStat.characterStats.stamine).ToList()[0].value;
        currHealth = characterStats.Where(stat => stat.type == SerializableCharacterStat.characterStats.health).ToList()[0].value;
        cards = new List<SerializablePowerCard>();
        torso.materials[0].mainTexture = opcs[Random.Range(0, 2)];
        for (int i = 0; i < Random.Range(2, 9); i++)
        {
            TattooData tempTattoo = DataManager.main.globalTattooData.tattoos[Random.Range(0, DataManager.main.globalTattooData.tattoos.Count() - 1)];
            tattoos.Add(tempTattoo);
            cards.Add(tempTattoo.powerCard);
        }
        foreach (var _tattoo in tattoos)
        {
            List<SerializableCharacterStat> tempHealthBoostValue = _tattoo.TattooStats.Where(t => t.type == SerializableCharacterStat.characterStats.health).ToList();
            List<SerializableCharacterStat> tempStamineBoostValue = _tattoo.TattooStats.Where(t => t.type == SerializableCharacterStat.characterStats.stamine).ToList();
            if (tempHealthBoostValue.Count>0)
            {
                currHealth +=tempHealthBoostValue[0].value;
            }
            if (tempStamineBoostValue.Count > 0)
            {
                currStamine += tempStamineBoostValue[0].value;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void GetDamaged(float _power)
    {
        currHealth -= _power;
    }
    public float GetCurrentHealth()
    {
        return currHealth;
    }
    public float GetCurrentStamine()
    {
        return currStamine;
    }
    public float GetPowerAttack()
    {
        List<SerializablePowerCard> selectedCards = new List<SerializablePowerCard>();
        float remainingStamine = currStamine;
        float totalPower = 0;
        for (int i = 0; i < Random.Range(1, 4); i++)
        {
            int tempRandPos = Random.Range(0, cards.Count);
            if (remainingStamine >= cards[tempRandPos].stamineCost)
            {
                selectedCards.Add(cards[tempRandPos]);
                AnimManager.main.AddEnemyAnim(cards[tempRandPos].animName);
            }
            remainingStamine -= cards[tempRandPos].stamineCost;
        }
        foreach (var _card in selectedCards)
        {
            totalPower += _card.power;
        }
        selectedEnemyCards = selectedCards;
        return totalPower;
    }
}
