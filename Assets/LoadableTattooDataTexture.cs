
using UnityEngine;

[System.Serializable]
public class LoadableTattooDataTexture
{
    [SerializeField]
    public string saveName;
    [SerializeField]
    public string pngData;
    public LoadableTattooDataTexture(string _saveName, string _pngData)
    {
        saveName = _saveName;
        pngData = _pngData;
    }
}
