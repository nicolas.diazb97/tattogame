using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIBattleManager : Singleton<UIBattleManager>
{
    public int prize;
    public Slider healthBar;
    public Slider stamineBar;
    public Slider enemyHealthBar;
    public Slider enemyStamineBar;

    public TextMeshProUGUI roundName;
    public Image timerImage;

    public GameObject endOfGameScreen;
    public TextMeshProUGUI winnerText;
    public TextMeshProUGUI prizeText;
    public GameObject enemyRender;
    public GameObject characterRender;
    public GameObject backgroundBrawlersHolder;
    public TextMeshProUGUI powerText;
    public TextMeshProUGUI enemyFinalPowerText;
    public TextMeshProUGUI FinalPowerText;

    // Start is called before the first frame update
    void Start()
    {
        DataManager.main.InitBattleValues();
        EnemyController.main.InitBaseValues();
        UpdateOwnBars(DataManager.main.GetCurrentHealth(), DataManager.main.GetCurrentStamine());
        UpdateEnemyBars(EnemyController.main.GetCurrentHealth(), EnemyController.main.GetCurrentStamine());

        RoundManager.main.onRoundEnd.AddListener((RoundResults _results) =>
        {
            UpdateOwnBars(_results.currHealth, DataManager.main.GetCurrentStamine());
            UpdateEnemyBars(_results.currEnemyHealth, EnemyController.main.GetCurrentStamine());
        });
        CardManager.main.onStamineChange.AddListener((float _stamine) =>
        {
            Debug.LogError("Attack power: " + CardManager.main.GetCurrPower());
            powerText.text = "Attack power: " + CardManager.main.GetCurrPower();
            UpdateOwnBars(DataManager.main.GetCurrentHealth(), _stamine);
        });
        CardManager.main.endOfGame.AddListener(() =>
        {
            GameIsEnded();
        });
    }
    public void UpdateFinalTexts(string _power,string _enemyPower)
    {
        FinalPowerText.text = _power;
        enemyFinalPowerText.text = _enemyPower;
    }
    public void ResetFinalTexts()
    {
        FinalPowerText.text = "";
        enemyFinalPowerText.text = "";

    }
    // Update is called once per frame
    void Update()
    {

    }
    public void GameIsEnded()
    {
        endOfGameScreen.SetActive(true);

        //el jugador le gano al enemigo
        Debug.LogError("resultados finales: " + DataManager.main.GetCurrentHealth() + " vs " + EnemyController.main.GetCurrentHealth());
        if (DataManager.main.GetCurrentHealth() < EnemyController.main.GetCurrentHealth())
        {
            winnerText.text = "Opponent win this time";
            prizeText.text = "";
            enemyRender.SetActive(true);
            characterRender.SetActive(false);
        }
        //el el enemigo le gano al jugador
        if (DataManager.main.GetCurrentHealth() > EnemyController.main.GetCurrentHealth())
        {
            DataManager.main.AddMoney(prize);
            prizeText.text = "Money + " + prize;
            winnerText.text = "You win this time";
            DataManager.main.UpdateGameData();
            enemyRender.SetActive(false);
            characterRender.SetActive(true);
        }
        //ees un empate
        if (DataManager.main.GetCurrentHealth() == EnemyController.main.GetCurrentHealth())
        {
            winnerText.text = "Its a draw!";
        }
    }
    public void ResetPowerText()
    {
        Debug.LogError("power text has been updated");
        powerText.text = "Attack power: 0";
    }
    public void UpdateOwnBars(float _health, float _stamine)
    {
        healthBar.value = _health.Remap(0, (int)SerializableCharacterStat.characterStats.health, 0.0000f, 1.0000f);
        stamineBar.value = _stamine.Remap(0, (int)SerializableCharacterStat.characterStats.stamine, 0.0000f, 1.0000f);
    }
    public void UpdateEnemyBars(float _health, float _stamine)
    {
        enemyHealthBar.value = _health.Remap(0, (int)SerializableCharacterStat.characterStats.health, 0.0000f, 1.0000f);
        enemyStamineBar.value = _stamine.Remap(0, (int)SerializableCharacterStat.characterStats.stamine, 0.0000f, 1.0000f);
    }
    public void UpdateRoundData(string _roundName)
    {
        roundName.text = _roundName;
    }
    public void SetTimer(float _amount)
    {
        timerImage.fillAmount = _amount;
    }
}
