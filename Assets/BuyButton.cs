using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyButton : Singleton<BuyButton>
{
    public GameObject orbitingState;
    // Start is called before the first frame update
    void Start()
    {
        ShopManager.main.OnTattooSelected.AddListener((TattooData _data) =>
        {
            if (!DataManager.main.CheckForOwnedTattooByName(_data.tattooName))
            {
                gameObject.SetActive(true);
                GetComponent<Button>().onClick.RemoveAllListeners();
                if (DataManager.main.CheckEnoughMoney(_data.cost))
                {
                    GetComponent<Button>().interactable = true;
                    GetComponent<Button>().onClick.AddListener(() =>
                    {
                        BuyTattoo(_data);
                    });
                }
                else
                {
                    GetComponent<Button>().interactable = false;
                }
            }
            else
            {
                gameObject.SetActive(false);
            }
        });
        gameObject.SetActive(false);
    }

    public void BuyTattoo(TattooData _data)
    {
        if (DataManager.main.CheckEnoughMoney(_data.cost))
        {
            DataManager.main.SpendMoney(_data.cost);
            UIShopManager.main.UpdateUI();
            DataManager.main.AddNewOwnedTattoo(_data.tattooName);
            gameObject.SetActive(false);
            ShopManager.main.CheckForCurrentTattooState(_data);
            orbitingState.SetActive(true);
            DataManager.main.UpdateGameData();
        }
    }
    public void RunFailPurchaseAnim()
    {
        GetComponent<Animator>().Play("fail");
    }
    // Update is called once per frame
    void Update()
    {

    }
}
