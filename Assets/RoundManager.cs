using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class RoundManager : Singleton<RoundManager>
{
    public List<Round> rounds;
    public int currRound;
    public float roundDuration;

    public OnRoundEnd onRoundEnd;
    // Start is called before the first frame update
    void Start()
    {
        rounds = GetComponentsInChildren<Round>(true).ToList();
        rounds[currRound].Init(roundDuration);
        onRoundEnd.AddListener((RoundResults _results) =>
        {
            SetRoundResults(_results);
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SetRoundResults(RoundResults _results)
    {
        rounds[currRound].SetResults(_results);
    }
    public void NextRound()
    {
        currRound++;
        if (currRound != 4)
        {
            rounds[currRound].Init(roundDuration);
        }
        else
        {
            CardManager.main.endOfGame.Invoke();
        }
    }
}
