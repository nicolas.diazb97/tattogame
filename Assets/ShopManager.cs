using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

public class ShopManager : Singleton<ShopManager>
{
    public TattoosData globalTattooData;
    public GameObject uiTattooPrefab;
    public TattoosContainer container;
    public TattooSelected OnTattooSelected;
    public OnTattooStateChecked onTattoStateChecked;
    // Start is called before the first frame update
    void Start()
    {
        LoadData();
        OnTattooSelected.AddListener((TattooData _data) => CheckForCurrentTattooState(_data));
    }

    // Update is called once per frame
    void Update()
    {

    }
    // revisar estado tattoo
    public void CheckForCurrentTattooState(TattooData _data)
    {
        if (DataManager.main.CheckForOwnedTattooByName(_data.tattooName))
        {
            onTattoStateChecked.Invoke(true);
        }
        else
        {
            onTattoStateChecked.Invoke(false);
        }
    }
    public void LoadData()
    {
        globalTattooData.tattoos.ToList().ForEach(tat =>
        {
            GameObject tempuiTattoo = Instantiate(uiTattooPrefab, container.transform);
            tempuiTattoo.GetComponent<Tattoo>().SetData(tat);
        });

    }
   
}

