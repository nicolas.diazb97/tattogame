using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataRefs : MonoBehaviour
{
    // Start is called before the first frame update

    public void UpdateChracter(TattooData _data)
    {
        DataManager.main.UpdateChracter(_data);
    }
    public void UpdateGameData()
    {
        DataManager.main.UpdateGameData();
    }
}
