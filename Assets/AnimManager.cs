using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimManager : Singleton<AnimManager>
{
    public List<string> anims;
    public List<string> enemyAnims;

    public Animator anim;
    public Animator enemyAnim;
    public GameObject texture;
    public CardHolder cardHolder;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ResetRoundAnimData()
    {
        anims.Clear();
        enemyAnims.Clear();
    }
    public void AddAnim(string _anim)
    {
        anims.Add(_anim);
    }
    public void AddEnemyAnim(string _anim)
    {
        enemyAnims.Add(_anim);
    }
    public void StartPlayingAnims()
    {
        texture.SetActive(true);
        StartCoroutine(WaitToGetBack());
        if (anims.Count > 0)
        {
            anim.Play(anims[0]);
        }
        if (enemyAnims.Count > 0)
        {
            enemyAnim.Play(enemyAnims[0]);
        }
    }
    IEnumerator WaitToGetBack()
    {
        yield return new WaitForSecondsRealtime(5f);
        UIBattleManager.main.ResetPowerText();
        cardHolder.cards.ForEach(_card => _card.DiselectCard());
        UIBattleManager.main.ResetFinalTexts();
        ResetRoundAnimData();
        RoundManager.main.NextRound();
        texture.SetActive(false);
    }
}
