using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UiHomeScreen : Singleton<UiHomeScreen>
{
    public TextMeshProUGUI user;
    // Start is called before the first frame update
    void Start()
    {
        user.text = DatabaseManager.main.displayName;
        DataManager.main.GetDatafromDB();
        DatabaseManager.main.UpdateLocalUserTextureFromDB();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
