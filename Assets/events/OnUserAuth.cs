using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Firebase.Auth;

[System.Serializable]
public class OnUserAuth : UnityEvent<Firebase.Auth.FirebaseUser>
{
   
}
