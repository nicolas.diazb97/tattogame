using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TattooSelected : UnityEvent<TattooData>
{
}