using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnTattoApply : UnityEvent<TattooData>
{
}