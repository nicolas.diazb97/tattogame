
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnTattooStateChecked : UnityEvent<bool>
{
    
}
