using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace PaintIn3D
{
    public class TextureHandler : MonoBehaviour
    {

        public P3dPaintableTexture characterTexture;
        public P3dPaintableTexture enemyTexture;
        // Start is called before the first frame update
        void Awake()
        {
            characterTexture.LoadFromDB(DatabaseManager.main.pngData);
            //enemyTexture.LoadFromDB(DatabaseManager.main.pngData);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
