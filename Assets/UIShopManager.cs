using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIShopManager : Singleton<UIShopManager>
{
    public GameObject panelInfo;
    public GameObject statsContainer;
    public TextMeshProUGUI currentMoney;
    public TextMeshProUGUI TattooName;
    public TextMeshProUGUI TattooCost;
    public TextMeshProUGUI TattooDescription;
    public UICharacterStatsHolder statsHolder;
    public Image frame;
    public Image tattooTexture;
    // Start is called before the first frame update
    void Start()
    {
        UpdateUI();
        ShopManager.main.OnTattooSelected.AddListener((TattooData _data) =>
        {
            ShowTattooData(_data);
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void UpdateUI()
    {
        currentMoney.text = "Money: "+DataManager.main.currMoney.ToString();
    }
    public void ShowTattooData(TattooData _data)
    {
        panelInfo.SetActive(true);
        TattooName.text = _data.tattooName;
        TattooCost.text = _data.cost.ToString();
        TattooDescription.text = _data.tattooDescription;
        frame.sprite = _data.powerCard.cardFrame;
        tattooTexture.sprite = _data.powerCard.cardIllustration;
        statsHolder.ShowTempTattooBoost(_data);
    }
}
