using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CardHolder : MonoBehaviour
{
    public List<PowerCard> cards;
    // Start is called before the first frame update
    void Start()
    {
    }
    public void InitCards()
    {
    }
    public void CheckCardsForCooldown()
    {
        if (cards.Count <= 0)
        {
            cards = GetComponentsInChildren<PowerCard>(true).ToList();
        }
        foreach (var _card in cards)
        {
            _card.CheckForCooldown();
        }
    }

}
